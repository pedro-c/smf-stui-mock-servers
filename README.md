# smf-stui-mock-servers

* The purpose of this project is to mock the API that will be used by the smf-client-lib to query
    * games
    * competitions
    * teams

```
​
+-----------+            +-----------+          +----------+
|    SMF    +----------->+    SMF    +--------->+   SMF    |
|    UI     |            |  BackEnd  |          | Mock API |
|           +<-----------+           +<---------+          |
+-----------+            +-----------+          +----------+

```

## Main Features
* Mock the Sports Entity Server that contain the API to fetch games, Compaetitions and Teams

# Getting Started

## Requirements
* JDK 11
* Maven

## How to use
* Build application ```mvn clean install```
* Run the Application
* Schema can be seen using [voyager](http://localhost:8082/voyager) or [graphiql](http://localhost:8082/graphiql)
* Queries can be done in [graphiql](http://localhost:8082/graphiql)

### Reference Documentation
The following guides illustrate the API that is being mocked and the diagrams:

* [Entity Query Service Interface](https://sportsbet.atlassian.net/wiki/spaces/ALIUM/pages/469270883/3.7.1.2.1+Entity+Query+Service+Interface+DRAFT)
* [Create Game](https://sportsbet.atlassian.net/wiki/spaces/ALIUM/pages/495027253/Create+Game)
