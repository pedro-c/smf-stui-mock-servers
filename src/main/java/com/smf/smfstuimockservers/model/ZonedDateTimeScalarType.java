package com.smf.smfstuimockservers.model;

import graphql.schema.*;
import org.springframework.stereotype.Component;

import java.time.DateTimeException;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;


@Component
public class ZonedDateTimeScalarType extends GraphQLScalarType {

    ZonedDateTimeScalarType() {
        super("ZonedDateTime", "ZonedDateTime", new Coercing<ZonedDateTime, String>() {
            @Override
            public String serialize(Object input) throws CoercingSerializeException {
                ZonedDateTime zonedDateTime;
                if (input instanceof ZonedDateTime) {
                    zonedDateTime = ((ZonedDateTime) input);
                } else {
                    throw new CoercingSerializeException(
                            "Expected something we can convert to 'java.time.ZonedDateTime' but was '" + input.getClass().getSimpleName() + "'."
                    );
                }
                try {
                    return DateTimeFormatter.ISO_ZONED_DATE_TIME.format(zonedDateTime);
                } catch (DateTimeException e) {
                    throw new CoercingSerializeException(
                            "Unable to turn TemporalAccessor into OffsetDateTime because of : '" + e.getMessage() + "'."
                    );
                }
            }

            @Override
            public ZonedDateTime parseValue(Object input) throws CoercingParseValueException {
                ZonedDateTime zonedDateTime;
                if (input instanceof ZonedDateTime) {
                    zonedDateTime = ((ZonedDateTime) input);
                } else {
                    throw new CoercingParseValueException(
                            "Expected a 'String' but was '" + input.getClass().getSimpleName() + "'."
                    );
                }
                return zonedDateTime;
            }

            @Override
            public ZonedDateTime parseLiteral(Object input) throws CoercingParseLiteralException {
                if (!(input instanceof String)) {
                    throw new CoercingParseLiteralException(
                            "Expected AST type 'StringValue' but was '" + input.getClass().getSimpleName() + "'."
                    );
                }
                return parseOffsetDateTime((String) input);
            }

            private ZonedDateTime parseOffsetDateTime(String s) {
                try {
                    return ZonedDateTime.parse(s, DateTimeFormatter.ISO_ZONED_DATE_TIME);
                } catch (DateTimeParseException e) {
                    throw new CoercingParseValueException(
                            "Invalid RFC3339 value : '" + s + "'. because of : '" + e.getMessage() + "'"
                    );
                }
            }
        });
    }

}