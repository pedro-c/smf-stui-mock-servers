package com.smf.smfstuimockservers.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NonNull;

@Data
@AllArgsConstructor(staticName = "of")
public class BasketballTeam {

    @NonNull
    private String id;
    private String name;
    private TeamHostDesignation type;

}
