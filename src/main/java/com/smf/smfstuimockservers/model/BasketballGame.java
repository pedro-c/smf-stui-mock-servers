package com.smf.smfstuimockservers.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NonNull;

import java.time.ZonedDateTime;
import java.util.List;

@Data
@AllArgsConstructor(staticName = "of")
public class BasketballGame {

    @NonNull
    private String id;
    private ZonedDateTime startTime;
    private List<BasketballTeam> teams;
    private String venue;
    private BasketballCompetition competition;

}
