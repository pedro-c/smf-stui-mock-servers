package com.smf.smfstuimockservers.model;

public enum TeamHostDesignation {
    AWAY,
    HOME,
    NEUTRAL
}
