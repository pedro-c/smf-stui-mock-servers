package com.smf.smfstuimockservers.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NonNull;

@Data
@AllArgsConstructor(staticName = "of")
public class BasketballCompetition {

    @NonNull
    private String Id;
    private String name;

}
