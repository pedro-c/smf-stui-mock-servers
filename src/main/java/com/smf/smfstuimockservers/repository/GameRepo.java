package com.smf.smfstuimockservers.repository;

import com.smf.smfstuimockservers.model.BasketballCompetition;
import com.smf.smfstuimockservers.model.BasketballGame;
import com.smf.smfstuimockservers.model.BasketballTeam;
import com.smf.smfstuimockservers.model.TeamHostDesignation;
import org.springframework.stereotype.Repository;

import java.time.ZonedDateTime;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Repository
public class GameRepo {

    Map<String, BasketballGame> basketballGames = new HashMap<>() {{
        ZonedDateTime zonedDateTime = ZonedDateTime.now();
        BasketballTeam basketballTeamA = BasketballTeam.of("1", "Davidson Wildcats", TeamHostDesignation.HOME);
        BasketballTeam basketballTeamB = BasketballTeam.of("2", "Dayton Flyers", TeamHostDesignation.AWAY);
        BasketballTeam basketballTeamC = BasketballTeam.of("3", "Duquesne Dukes", TeamHostDesignation.AWAY);
        BasketballCompetition competition1 = BasketballCompetition.of("1", "The NCAA Championships");

        BasketballGame game1 = BasketballGame.of("1", zonedDateTime, List.of(basketballTeamA, basketballTeamB), "1", competition1);
        put(game1.getId(), game1);
        BasketballGame game2 = BasketballGame.of("2", zonedDateTime, List.of(basketballTeamA, basketballTeamC), "1", competition1);
        put(game2.getId(), game2);
    }};

    public Collection<BasketballGame> getAllCBasketballGames() {
        return basketballGames.values();
    }

    public BasketballGame getBasketballGameById(String id) {
        return basketballGames.get(id);
    }
}
