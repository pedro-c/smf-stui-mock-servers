package com.smf.smfstuimockservers.repository;

import com.smf.smfstuimockservers.model.BasketballCompetition;
import org.springframework.stereotype.Repository;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

@Repository
public class CompetitionRepo {

    Map<String, BasketballCompetition> competitions = new HashMap<>() {{
        BasketballCompetition competition1 = BasketballCompetition.of("1", "The NCAA Championships");
        put(competition1.getId(), competition1);
        BasketballCompetition competition2 = BasketballCompetition.of("2", "The National Invitational Tournament");
        put(competition2.getId(), competition2);
        BasketballCompetition competition3 = BasketballCompetition.of("3", "The College Basketball Invitational Tournament");
        put(competition3.getId(), competition3);
        BasketballCompetition competition4 = BasketballCompetition.of("4", "The CollegeInsider.com Tournament");
        put(competition4.getId(), competition4);
    }};

    public Collection<BasketballCompetition> getAllCompetitions() {
        return competitions.values();
    }

    public BasketballCompetition getCompetitionById(String id) {
        return competitions.get(id);
    }

}
