package com.smf.smfstuimockservers.repository;

import com.smf.smfstuimockservers.model.BasketballTeam;
import com.smf.smfstuimockservers.model.TeamHostDesignation;
import org.springframework.stereotype.Repository;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

@Repository
public class TeamRepo {

    Map<String, BasketballTeam> teams = new HashMap<>() {{
        BasketballTeam team1 = BasketballTeam.of("1", "Davidson Wildcats", TeamHostDesignation.NEUTRAL);
        put(team1.getId(), team1);
        BasketballTeam team2 = BasketballTeam.of("2", "Dayton Flyers", TeamHostDesignation.NEUTRAL);
        put(team2.getId(), team2);
        BasketballTeam team3 = BasketballTeam.of("3", "Duquesne Dukes", TeamHostDesignation.NEUTRAL);
        put(team3.getId(), team3);
        BasketballTeam team4 = BasketballTeam.of("4", "Fordham Rams", TeamHostDesignation.NEUTRAL);
        put(team4.getId(), team4);
    }};

    public Collection<BasketballTeam> getAllTeams() {
        return teams.values();
    }

    public BasketballTeam getTeamById(String id) {
        return teams.get(id);
    }

}
