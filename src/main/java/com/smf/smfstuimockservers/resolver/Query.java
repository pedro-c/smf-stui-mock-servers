package com.smf.smfstuimockservers.resolver;

import com.coxautodev.graphql.tools.GraphQLQueryResolver;
import com.smf.smfstuimockservers.model.BasketballCompetition;
import com.smf.smfstuimockservers.model.BasketballGame;
import com.smf.smfstuimockservers.model.BasketballTeam;
import com.smf.smfstuimockservers.repository.CompetitionRepo;
import com.smf.smfstuimockservers.repository.GameRepo;
import com.smf.smfstuimockservers.repository.TeamRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Collection;

@Component
public class Query implements GraphQLQueryResolver {

    @Autowired
    private CompetitionRepo competitionRepo;
    @Autowired
    private GameRepo gameRepo;
    @Autowired
    private TeamRepo teamRepo;

    public BasketballTeam team(String teamId){
        return teamRepo.getTeamById(teamId);
    }

    public Collection<BasketballTeam> teams(){
        return teamRepo.getAllTeams();
    }

    public BasketballCompetition competition(String competitionId){
        return competitionRepo.getCompetitionById(competitionId);
    }

    public Collection<BasketballCompetition> competitions(){
        return competitionRepo.getAllCompetitions();
    }

    public BasketballGame game(String gameId){
        return gameRepo.getBasketballGameById(gameId);
    }

    public Collection<BasketballGame> games(){
        return gameRepo.getAllCBasketballGames();
    }

}
